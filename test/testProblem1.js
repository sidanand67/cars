const inventory = require('../inventory'); 
const problem1 = require('../problem1'); 

let carId = 33; 

const result = problem1(inventory, carId); 

if (Object.keys(result).length !== 0){
    console.log(`Car ${result.id} is a ${result.car_year} ${result.car_make} ${result.car_model}`); 
}
else {
    console.log([]);
}