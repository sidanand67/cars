function problem2(inventory){
    let lastCarId = inventory.length-1; 
    let carMake = inventory[lastCarId].car_make; 
    let carModel = inventory[lastCarId].car_model; 
    return `Last car is a ${carMake} ${carModel}`;
}

module.exports = problem2; 