function problem6(inventory){
    let bmwAndAudi = []; 
    for(let i = 0; i < inventory.length; i++){
        if (inventory[i].car_make === "Audi" || inventory[i].car_make === "BMW"){
            bmwAndAudi.push(inventory[i]); 
        }
    }
    return JSON.stringify(bmwAndAudi); 
}

module.exports = problem6