function problem1(inventory, carId){

    if (Array.isArray(inventory) === false || inventory === undefined || inventory.length === 0 || carId === undefined){
        return []; 
    }
    else {
        for(let i = 0; i < inventory.length; i++){
            if(inventory[i].id === carId){
                return inventory[i]; 
            }
        }
        return []; 
    }
}

module.exports = problem1; 


